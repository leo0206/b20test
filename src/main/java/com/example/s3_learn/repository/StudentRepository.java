package com.example.s3_learn.repository;

import com.example.s3_learn.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Integer> {

    boolean existsById(Integer id);
}
