package com.example.s3_learn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S3LearnApplication {
	public static void main(String[] args) {
		SpringApplication.run(S3LearnApplication.class, args);
	}
}
