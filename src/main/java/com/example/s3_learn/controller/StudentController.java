package com.example.s3_learn.controller;

import com.example.s3_learn.entity.Student;
import com.example.s3_learn.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping
@RequiredArgsConstructor
public class StudentController {

    private final StudentRepository studentRepository;

    @GetMapping("/getAll")
    public ResponseEntity<?> getAll(){
        List<Student> all = studentRepository.findAll();
        return ResponseEntity.ok(all);
    }

    @PostMapping("/add")
    public ResponseEntity<?> add(@RequestBody Student student){
        if(!studentRepository.existsById(student.getId())){
            studentRepository.save(student);
            return ResponseEntity.status(200).build();
        }


        return new ResponseEntity<>("mavjud", HttpStatus.BAD_REQUEST);
    }
}
