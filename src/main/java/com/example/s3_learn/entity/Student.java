package com.example.s3_learn.entity;

import lombok.*;
import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Student {
    @Id
    private Integer id;

    @Column
    private String name;
}





